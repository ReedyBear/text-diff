<?php

namespace DecaturVote\Differ\Test;

use \DecaturVote\Differ;

class TextDiffs extends \Tlf\Tester {

    protected function get_random_str(int $length){
        $i = 0;
        $str = '';
        $chars = str_split("abcdefghijklmnopqrstuv\n\n\n\n");
        // $chars = str_split("abc\n");
        $max = count($chars)-1;
        while ($i++<$length){
            $str .= $chars[random_int(0,$max)];
        }

        return $str;
    }

    public function testLargeRandomStringDiffs(){
        $old = $this->get_random_str(15000);
        $new = $this->get_random_str(15000);
        // echo "\n\n\n-----------\nOLD\n";
        // var_export($old);
        // echo "\n-----------\nNEW\n";
        // var_export($new);
        // echo "\n-----------\n\n";

        $differ = new Differ();
        $opps = $differ->get_opps($old, $new);

        $this->test("Opps");
        // $this->compare_lines(
        //     "0-One
        //      0+Zero
        //      1+Ones
        //      3+Two.Five
        //     ",
        //     $opps
        // );

        $this->test("Old to new with opps");
        $new_from_opps = $differ->forward($old, $opps);
        $this->compare($new,$new_from_opps);


        $this->test("New to old with opps");
        $old_from_opps = $differ->backward($new, $opps);
        $this->compare($old,$old_from_opps);
    }

    /**
     * Testing a large set of different diff setups
     * @usage phptest -test MultipleDiffs
     * @usage phptest -test MultipleDiffs -run dup ... where `dup` is the key for the diff test to run
     */
    public function testMultipleDiffs(){
        $run = $this->cli->args['run'] ?? true;
        $diffs = 
        [
            [ 'name'=>'Remove only line',
            'old'=>'abc',
            'new'=>'',
            'opps'=>"0-abc\n0+",
            ],
            ['name'=>'add only line',
            'old'=>'',
            'new'=>'def',
            'opps'=>"0-\n0+def",
            ],
            ['name'=>'Add start line',
            'old'=>"B\nC",
            'new'=>"A\nB\nC",
            'opps'=>"0+A",
            ],
            ['name'=>'Add middle line',
            'old'=>"A\nC",
            'new'=>"A\nB\nC",
            'opps'=>"1+B",
            ],
            ['name'=>'Add end line',
            'old'=>"A\nB",
            'new'=>"A\nB\nC",
            'opps'=>"2+C",
            ],
            ['name'=>'Add beginning + middle + end',
            'old'=>"B\nD",
            'new'=>"A\nB\nC\nD\nE",
            'opps'=>"0+A\n2+C\n4+E",
            ],
            ['name'=>'Remove 2nd & 3rd lines',
            'old'=>"A\nB\nC",
            'new'=>"A",
            'opps'=>"1-B\n1-C",
            ],
            ['name'=>'Remove beginning + middle + end',
            'old'=>"A\nB\nC\nD\nE",
            'new'=>"B\nD",
            'opps'=>"0-A\n1-C\n2-E",
            ],
            'dup'=>['name'=>'Add Duplicate Line',
            'old'=>"A",
            'new'=>"A\nA",
            'opps'=>"1+A",
            ],
            'dup2'=>['name'=>'Add Duplicate Lines AFTER',
            'old'=>"A\nB",
            'new'=>"A\nB\nA\nB",
            'opps'=>"2+A\n3+B",
            ],
            'dup3'=>['name'=>'Add Duplicate Lines INTRA',
            'old'=>"A\nB",
            'new'=>"A\nA\nB\nB",
            'opps'=>"1+A\n3+B",
            ],
            'move'=>['name'=>'Move Lines',
            'old'=>"D\nC\nB\nA",
            'new'=>"A\nD\nC\nB",
            'opps'=>"0>1\n1>2\n2>3\n3>0",
            ],
            'reverse'=>['name'=>'Reverse Lines',
            'old'=>"D\nC\nB\nA",
            'new'=>"A\nB\nC\nD",
            'opps'=>"0>3\n1>2\n2>1\n3>0",
            ],
            'move_dups'=>['name'=>'Move with duplicates',
            'old'=>"A\nB\nC\nA\nB\nC",
            'new'=>"A\nC\nB\nA\nB\nC",
            'opps'=>"1>2\n2>1",
            ],
            'move_dups2'=>['name'=>'Move with duplicates #2',
            'old'=>"A\nB\nC\nA\nB\nC",
            'new'=>"A\nC\nB\nA\nC\nB",
            'opps'=>"1>2\n2>1\n4>5\n5>4",
            ],
            'rem_add_mov'=>['name'=>'Remove, Add, & Move Lines',
                'old'=>"A\nB\nC\nD\nE",
                'new'=>"F\nB\nA\nD\nC\nG",
                "opps"=>"4-E\n0+F\n5+G\n1>2\n2>1\n3>4\n4>3"
            ],
            'add_dup_and_mov'=>['name'=>'Add Duplicates & Move lines',
                'old'=>"A\nB\nC",
                'new'=>"B\nA\nC\nB",
                "opps"=>"3+B\n0>1\n1>0"
            ],
            'add_dup_and_mov2'=>['name'=>'Add Duplicates & Move lines, #2',
                'old'=>"A\nB\nC",
                'new'=>"B\nA\nB\nC\nB",
                "opps"=>"2+B\n4+B\n0>1\n1>0"
            ],
            'mish_mosh'=>['name'=>'A bunch of stuff, idk',
                'old'=>"a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\no\np",
                'new'=>"q\na\np\nd\nh\nc\ng\ni\nh\na\nl\no\no\no\np\nb",
                'opps'=>"4-e\n4-f\n7-j\n7-k\n8-m\n0+q\n8+h\n9+a\n12+o\n13+o\n14+p\n2>15\n3>5\n4>3\n5>6\n6>4\n15>2"
            ],
            'newline_start'=>['name'=>'Old starts with a new line',
                'old'=>"\nusk\nmo",
                'new'=>"v\nnkt",
                'opps'=>"0-\n0-usk\n0-mo\n0+v\n1+nkt"
            ],
            'blanks'=>['name'=>'Remove duplicate blank line',
                'old'=>"\n",
                'new'=>"",
                'opps'=>'1-',
            ],
            'remove_dup'=>['name'=>'Remove duplicate text line',
                'old'=>"a\na",
                'new'=>'a',
                'opps'=>"1-a"
            ],
            //@bug: lines_to_add was not correctly updating the offset, but now it is
            'dup_bug1'=>['name'=>'Duplicates Bug #1',
                'old'=>"a\n\n",
                'new'=>"\n\n\nb",
                "opps"=>"0-a\n2+\n3+b"
            ],
            'bug2' => ['name'=>'Bug #2', // fixed! I was overwriting the offset on lines_to_remove when it should have stayed the same
                'old'=>"\na\nb\nc\n\n\nd\ne\nf\ng\nh\n\ni\n",
                'new'=>"k\nl\nm\nn\no\np\nq\n\nr",
                'opps'=>"1-a\n1-b\n1-c\n1-\n1-\n1-d\n1-e\n1-f\n1-g\n1-h\n1-\n1-i\n1-\n0+k\n1+l\n2+m\n3+n\n4+o\n5+p\n6+q\n8+r",
            ],
            'bug3' => ['name'=>'Bug #3', // fixed! a letter is getting inserted where a blank line should be
                'old'=>"baccc\n\naaa\nb\naabcab",
                'new'=>"cababc\n\n\na\ncbbacbc\nb",
                'opps'=>"0-baccc\n1-aaa\n2-aabcab\n0+cababc\n2+\n3+a\n4+cbbacbc",
            ],
            'bug4' => ['name'=>'Bug #4', // fixed! the move operations were moving a blank line to where a blank line already existed. the move code for get_opps was not considering a duplicate being moved to a like-spot
                'old'=>"\nb\nacbc\na\nbbccb\n\naacc\nbccc\ncac",
                'new'=>"cac\naa\ncba\nbbac\nabbaa\n\n\n\nbcacb",
                'opps'=>"1-b\n1-acbc\n1-a\n1-bbccb\n2-aacc\n2-bccc\n1+aa\n2+cba\n3+bbac\n4+abbaa\n7+\n8+bcacb\n0>6\n6>0",
            ],
            'bug5' => ['name'=>'Bug #5', // 
                'old'=>"cc\nbacbca\na\n\nac\nb\n\n\nbaa\n\n\na\n\nb\n\ncb\n\nabb\n",
                'new'=>"a\nbc\nb\ncc\n\n\nc\nac\nc\na\na\ncab\n\nbbbaabcaca\n\n",
                'opps'=> "1-bacbca\n7-baa\n10-\n10-b\n10-\n10-cb\n10-\n10-abb\n10-\n1+bc\n6+c\n8+c\n10+a\n11+cab\n13+bbbaabcaca\n0>3\n2>0\n3>4\n4>7\n5>2\n7>5\n9>15\n15>9"
            ],



        ];

        $differ = new Differ();
        $failures = [];
        foreach ($diffs as $key=>$test){
            if ($run!==true && $run !== $key)continue;
            $old = $test['old'];
            $new = $test['new'];
            $opps = $test['opps'];
            $this->test($test['name']);

            echo "\n::Opps::";
            $actual_opps = $differ->get_opps($old, $new);
            $this->compare($opps, $actual_opps, true)
                ? '' : $failures[] = 'opps '.$test['name'];

            echo "\n::Forward::";
            $actual_new = $differ->forward($old,$actual_opps);
            $this->compare($new, $actual_new, true)
                ? '' : $failures[] = 'forward '.$test['name'];

            echo "\n::Backward::";
            $actual_old = $differ->backward($new, $actual_opps);
            $this->compare($old, $actual_old, true)
                ? '' : $failures[] = 'backward '.$test['name'];
        }
        
        echo "\n\n\n-----------\nFAILURES\n";
        print_r($failures);
    }

    /**
     * Standalone test of one input/output
     */
    public function testMainDiff(){
        $differ = new Differ();

        // base strings
        $old = "One\nTwo\nThree";
        $new = "Zero\nOnes\nTwo\nTwo.Five\nThree";
        $opps = $differ->get_opps($old, $new);

        $this->test("Opps");
        $this->compare_lines(
            "0-One
             0+Zero
             1+Ones
             3+Two.Five
            ",
            $opps
        );

        $this->test("Old to new with opps");
        $new_from_opps = $differ->forward($old, $opps);
        $this->compare($new,$new_from_opps);


        $this->test("New to old with opps");
        $old_from_opps = $differ->backward($new, $opps);
        $this->compare($old,$old_from_opps);
    }
}

